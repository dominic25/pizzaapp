package com.icbt.libraryapp2.utility;

public class Constants {
    public static final String KEY_ACTION = "action";
    public static final String KEY_USERNAME = "username";
    public static final String KEY_AGE = "age";
    public static final String KEY_ID = "id";

}
