package com.icbt.libraryapp2.utility;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class FirebaseHelper {

    public static final DatabaseReference PIZZA_ORDER_REFERENCE
            = FirebaseDatabase.getInstance().getReference("pizzaOrder");
    public static final DatabaseReference PIZZA_LIST_REFRENCE
            = FirebaseDatabase.getInstance().getReference("pizzaList");

}
