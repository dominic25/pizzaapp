package com.icbt.libraryapp2.model;

public class User {
    public String username;
    public String age;
    public String id;

    public User() {
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof User &&
                ((User) obj).id.contentEquals(id);
    }

    public boolean isChanged(User user) {
        return !user.username.contentEquals(this.username) ||
                !user.age.contentEquals(this.age);
    }
}
