package com.icbt.libraryapp2.model;

public class Order {
    private String status;
    private String orderId;
//    private String customerId;


    public Order() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
}
