package com.icbt.libraryapp2.ui.admin;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DatabaseReference;
import com.icbt.libraryapp2.R;
import com.icbt.libraryapp2.adapters.pizza.PizzaRecyclerAdapter;
import com.icbt.libraryapp2.model.Pizza;
import com.icbt.libraryapp2.utility.FirebaseHelper;

import java.util.ArrayList;

public class PizzaOrderActivity extends AppCompatActivity {
    DatabaseReference pizzaListReference;
    private RecyclerView rcOrders;
    private PizzaRecyclerAdapter pizzaRecyclerAdapter;
    private ArrayList<Pizza> pizzaArrayList;
    ChildEventListener pizzaChildEventLi

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pizza_order);

        rcOrders = findViewById(R.id.rcOrders);
        pizzaListReference = FirebaseHelper.PIZZA_LIST_REFRENCE;
        configureRecyclerView();


        loadPizzaListFromFirebase();
    }

    private void loadPizzaListFromFirebase() {

    }

    private void configureRecyclerView() {
        pizzaArrayList = new ArrayList<>();
        PizzaRecyclerAdapter pizzaRecyclerAdapter
                = new PizzaRecyclerAdapter(pizzaArrayList);
        rcOrders.setLayoutManager(new LinearLayoutManager(this));
        rcOrders.setAdapter(pizzaRecyclerAdapter);
    }

    private ArrayList<Pizza> getDummyPizzaList() {
        pizzaArrayList = new ArrayList<>();
        for(int i=0;i<100;i++){
            Pizza pizza = new Pizza();
            pizza.setId("id "+i);
            pizza.setName("name "+i);
            pizza.setPrice("price "+i);
            pizza.setSize("size "+i);
            pizzaArrayList.add(pizza);
        }
        return pizzaArrayList;
    }
}
