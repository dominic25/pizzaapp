package com.icbt.libraryapp2.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.icbt.libraryapp2.ui.member.MemberActivity;
import com.icbt.libraryapp2.R;
import com.icbt.libraryapp2.ui.admin.AdminActivity;

public class LoginActivity extends AppCompatActivity {

    TextInputEditText tetUser1;
    TextInputEditText tetPass1;
    Button btLogin;

    String username;
    String password;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        tetUser1 = findViewById(R.id.tetUser);
        tetPass1 = findViewById(R.id.tetPass);
        btLogin = findViewById(R.id.B1);


        btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isUserAdmin(getUsername(), getPassword())) {
                    startAdminActivity();
                } else if (isUserMember(getUsername(), getPassword())) {
                    startMemberActivity();

                }
            }
        });




    }
    private void startMemberActivity(){
        Intent intent=new Intent(LoginActivity.this, MemberActivity.class);
        startActivity(intent);
    }

    private void startAdminActivity(){
        Intent intent=new Intent(LoginActivity.this,AdminActivity.class);
        startActivity(intent);
    }


    private boolean isUserMember(String username, String password) {
        return username.contentEquals("member1") & password.contentEquals("4321");
    }
    private boolean isUserAdmin(String username, String password) {
        return username.contentEquals("admin") & password.contentEquals("1234");
    }

    private String getUsername(){
        return tetUser1.getText().toString();
    }

    private String getPassword(){
        return tetPass1.getText().toString();
    }
    private boolean isUserValid(String username, String password) {
        return username.contentEquals("admin") & password.contentEquals("1234");
    }
    private void startMainActivity(){
        Intent intent=new Intent(LoginActivity.this,MainActivity.class);
        startActivity(intent);
    }


}




