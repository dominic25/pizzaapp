package com.icbt.libraryapp2.ui.admin;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.icbt.libraryapp2.R;
import com.icbt.libraryapp2.ui.dialogs.ManageUsersDialog;

public class AdminActivity extends AppCompatActivity {
    Button btManageUser;
    Button btMangeOrders;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);

        btManageUser=findViewById(R.id.button1);
        btMangeOrders =findViewById(R.id.button2);

        btManageUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ManageUsersDialog manageUsersDialog=new ManageUsersDialog();

                manageUsersDialog.show(getSupportFragmentManager(),"tag");
            }
        });

        btMangeOrders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(AdminActivity.this,PizzaOrderActivity.class);
                startActivity(intent);
            }
        });
    }





}
