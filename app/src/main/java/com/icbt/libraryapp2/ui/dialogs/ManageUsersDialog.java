package com.icbt.libraryapp2.ui.dialogs;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.icbt.libraryapp2.ui.admin.AddMemberActivity;
import com.icbt.libraryapp2.R;
import com.icbt.libraryapp2.ui.member.ViewMemberActivity;

import static com.icbt.libraryapp2.utility.Constants.KEY_ACTION;

public class ManageUsersDialog extends BottomSheetDialogFragment {
    Button btAddMember;
    Button btViewMember;
    Button btEditMember;
    Button btDeleteMember;

    public ManageUsersDialog(){}

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view=inflater.inflate(R.layout.bottomsheet_manage_users,container,false);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btAddMember=view.findViewById(R.id.btAddMember);
        btViewMember=view.findViewById(R.id.btViewMember);
        btEditMember=view.findViewById(R.id.btEditMember);
        btDeleteMember=view.findViewById(R.id.btDeleteMember);

        btAddMember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(),AddMemberActivity.class);
                intent.putExtra(KEY_ACTION,"add");
                startActivity(intent);
            }
        });

        btViewMember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(),ViewMemberActivity.class);
                startActivity(intent);
            }
        });

    }


}
