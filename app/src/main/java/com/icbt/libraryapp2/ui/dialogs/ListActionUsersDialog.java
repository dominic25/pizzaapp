package com.icbt.libraryapp2.ui.dialogs;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.icbt.libraryapp2.ui.admin.AddMemberActivity;
import com.icbt.libraryapp2.R;

import static com.icbt.libraryapp2.utility.Constants.KEY_ACTION;
import static com.icbt.libraryapp2.utility.Constants.KEY_AGE;
import static com.icbt.libraryapp2.utility.Constants.KEY_ID;
import static com.icbt.libraryapp2.utility.Constants.KEY_USERNAME;

public class ListActionUsersDialog extends BottomSheetDialogFragment {
    Button btEditUser;
    Button btDeleteUser;

    DatabaseReference userReference;

    public ListActionUsersDialog() {}

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        userReference = FirebaseDatabase.getInstance().getReference("user");
        View view= inflater.inflate(R.layout.bottomsheet_list_action_user,container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btEditUser=view.findViewById(R.id.btEditUser);
        btDeleteUser=view.findViewById(R.id.btDeleteUser);

        Bundle bundle=getArguments();
        String userId= bundle.getString("userId");
        String userName=bundle.getString("userName");
        String age = bundle.getString(KEY_AGE);

        btDeleteUser.setText("Delete "+userName);
        btEditUser.setText("Edit "+userName);



        btEditUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), AddMemberActivity.class);
                intent.putExtra(KEY_ACTION,"edit");
                intent.putExtra(KEY_ID,userId);
                intent.putExtra(KEY_USERNAME,userName);
                intent.putExtra(KEY_AGE,age);
                startActivity(intent);
            }
        });

        btDeleteUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userReference.child(userId)
                        .setValue(null).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Toast.makeText(getActivity(),
                                "Successfully deleted",
                                Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });



    }
}
