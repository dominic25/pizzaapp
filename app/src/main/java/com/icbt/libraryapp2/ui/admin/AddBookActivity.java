package com.icbt.libraryapp2.ui.admin;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.icbt.libraryapp2.R;
import com.icbt.libraryapp2.model.Book;


public class AddBookActivity extends AppCompatActivity {
    EditText etBookname;
    EditText etId;

    Button B1;

    DatabaseReference userDatabaseReference;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_book);
        etBookname = findViewById(R.id.etBookname);
        etId = findViewById(R.id.etId);

        B1= findViewById(R.id.B1);

        userDatabaseReference= FirebaseDatabase.getInstance().getReference("Book");


        B1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username= etBookname.getText().toString();

                String id=etId.getText().toString();

                Book book =new Book();

                book.Bookname=username;
                book.id=id;

                userDatabaseReference.push().setValue(book).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(AddBookActivity.this, "Successfully Added!", Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });
    }
}
