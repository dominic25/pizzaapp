package com.icbt.libraryapp2.ui.member;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.icbt.libraryapp2.R;
import com.icbt.libraryapp2.adapters.MembersRecyclerAdapter;
import com.icbt.libraryapp2.model.User;

import java.util.ArrayList;

public class ViewMemberActivity extends AppCompatActivity {
    RecyclerView rcMembers;
    ArrayList<User> mUserArrayList;
    DatabaseReference userReference;
    MembersRecyclerAdapter membersRecyclerAdapter;
    ChildEventListener mChildEventListener;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_member);

        rcMembers = findViewById(R.id.rcMembers);

        userReference = FirebaseDatabase.getInstance().getReference().child("user");
        configureRecyclerView();
        getUserListFromFirebase();

    }

    @Override
    protected void onPause() {
        super.onPause();
        userReference.removeEventListener(mChildEventListener);
    }

    @Override
    protected void onResume() {
        super.onResume();
        userReference.addChildEventListener(mChildEventListener);
    }

    private void getUserListFromFirebase() {
        mChildEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                User newUser = dataSnapshot.getValue(User.class);

                if (mUserArrayList.contains(newUser)) {
                    int index = mUserArrayList.indexOf(newUser);
                    User oldUser = mUserArrayList.get(index);

                    if (oldUser.isChanged(newUser)) {
                        mUserArrayList.remove(oldUser);
                        mUserArrayList.add(newUser);
                    }
                } else {
                    mUserArrayList.add(newUser);

                }
                membersRecyclerAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                User user = dataSnapshot.getValue(User.class);
                for (User user1 : mUserArrayList) {
                    if (user1.id.contentEquals(user.id)) {
                        mUserArrayList.remove(user1);
                    }
                }

                mUserArrayList.add(user);
                membersRecyclerAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                mUserArrayList.remove(user);
                membersRecyclerAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };

    }

    private void configureRecyclerView() {
        mUserArrayList = new ArrayList<>();
        membersRecyclerAdapter = new MembersRecyclerAdapter(mUserArrayList,
                getSupportFragmentManager());
        rcMembers.setLayoutManager(new LinearLayoutManager(this));
        rcMembers.setAdapter(membersRecyclerAdapter);
    }

    private ArrayList<User> getEmptyList() {
        mUserArrayList = new ArrayList<>();
        return mUserArrayList;
    }

    private ArrayList<User> getDummyList() {
        ArrayList<User> users = new ArrayList<>();

        for (int i = 10; i < 200; i++) {
            User user = new User();
            user.username = "User name" + i;
            user.age = "User Age" + i;
            user.id = "User id" + i;

            users.add(user);
        }
        return users;
    }
}
