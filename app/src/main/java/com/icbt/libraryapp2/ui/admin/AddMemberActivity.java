package com.icbt.libraryapp2.ui.admin;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.icbt.libraryapp2.R;
import com.icbt.libraryapp2.model.User;

import static com.icbt.libraryapp2.utility.Constants.KEY_ACTION;
import static com.icbt.libraryapp2.utility.Constants.KEY_AGE;
import static com.icbt.libraryapp2.utility.Constants.KEY_ID;
import static com.icbt.libraryapp2.utility.Constants.KEY_USERNAME;


public class AddMemberActivity extends AppCompatActivity {

    EditText etUsername;
    EditText etId;
    EditText etAge;
    Button B1,editUserButton;

    DatabaseReference userDatabaseReference;
    private String userName;
    private String id;
    private String age;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addmember);
        etUsername = findViewById(R.id.etUsername);
        etId = findViewById(R.id.etId);
        etAge = findViewById(R.id.etAge);
        B1 = findViewById(R.id.B1);
        editUserButton = findViewById(R.id.btEditMember);

        String action = getIntent().getStringExtra(KEY_ACTION);
        if (action.contentEquals("add")){
            editUserButton.setVisibility(View.INVISIBLE);
        }else {
            B1.setVisibility(View.INVISIBLE);
            etId.setFocusable(false );

            userName = getIntent().getStringExtra(KEY_USERNAME);
            id = getIntent().getStringExtra(KEY_ID);
            age = getIntent().getStringExtra(KEY_AGE);

            etUsername.setText(userName);
            etId.setText(id);
            etAge.setText(age);

        }


        userDatabaseReference = FirebaseDatabase.getInstance().getReference("user");


        B1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = etUsername.getText().toString();
                String age = etAge.getText().toString();
                String id = etId.getText().toString();

                User user = new User();
                user.age = age;
                user.username = username;
                user.id = id;

                userDatabaseReference.child(user.id).setValue(user).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(AddMemberActivity.this, "Successfully Added!", Toast
                                .LENGTH_SHORT).show();
                    }
                });

            }
        });

        editUserButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String username = etUsername.getText().toString();
                String age = etAge.getText().toString();
                String id = etId.getText().toString();

                User user = new User();
                user.age = age;
                user.username = username;
                user.id = id;

                userDatabaseReference.child(user.id).setValue(user).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(AddMemberActivity.this, "Successfully Edited!", Toast
                                .LENGTH_SHORT).show();
                    }
                });

            }
        });
    }
}
