package com.icbt.libraryapp2.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.icbt.libraryapp2.R;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    private static final String TAG ="MainActivity" ;

    Button sendButton;
    EditText userInput;
    TextView outPutView;

    DatabaseReference db;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sendButton=findViewById(R.id.btOK);
        userInput=findViewById(R.id.etName);
        outPutView=findViewById(R.id.tvName);

        sendButton.setOnClickListener(this);
        outPutView.setOnClickListener(this);



        db= FirebaseDatabase.getInstance().getReference("name");



    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btOK:


                startHomeActivity();
                break;
            case R.id.tvName:
                //TODO
                    break;
        }
    }



    private void startHomeActivity(){
        Intent intent=new Intent(MainActivity.this,HomeActivity.class);
                        startActivity(intent);

    }
    private void showInput(){

        outPutView.setText(getUserInput());

    }




    private String getUserInput() {
        String input=userInput.getText().toString();
        return input;
    }
}
