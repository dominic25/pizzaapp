package com.icbt.libraryapp2.adapters.pizza;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.icbt.libraryapp2.R;

public class PizzaViewHolder extends RecyclerView.ViewHolder {
    public TextView tvPizzaName,tvPizzaPrice;

    public PizzaViewHolder(View itemView) {
        super(itemView);
        tvPizzaName = itemView.findViewById(R.id.tvPizzaName);
        tvPizzaPrice = itemView.findViewById(R.id.tvPizzaPrize);
    }

}
