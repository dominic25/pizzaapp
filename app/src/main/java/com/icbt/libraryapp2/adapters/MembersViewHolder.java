package com.icbt.libraryapp2.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.icbt.libraryapp2.R;
import com.icbt.libraryapp2.model.Order;

public class MembersViewHolder extends RecyclerView.ViewHolder {

    TextView tvUername;
    TextView tvAge;
    TextView tvId;


    public MembersViewHolder(View itemView) {
        super(itemView);
        tvUername=itemView.findViewById(R.id.tvUsername);
        tvAge=itemView.findViewById(R.id.tvAge);
        tvId=itemView.findViewById(R.id.tvId);

    }
}
