package com.icbt.libraryapp2.adapters;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.icbt.libraryapp2.R;
import com.icbt.libraryapp2.model.User;
import com.icbt.libraryapp2.ui.dialogs.ListActionUsersDialog;

import java.util.ArrayList;

import static com.icbt.libraryapp2.utility.Constants.KEY_AGE;

public class MembersRecyclerAdapter extends RecyclerView.Adapter<MembersViewHolder>{
    ArrayList<User> mUserArrayList;
    FragmentManager mFragmentManager;

    private MembersRecyclerAdapter(){};

    public MembersRecyclerAdapter(ArrayList<User> userArrayList,
                                  FragmentManager supportFragmentManager){
        mUserArrayList = userArrayList;
        mFragmentManager=supportFragmentManager;


    }

    @NonNull
    @Override
    public MembersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater=LayoutInflater.from(parent.getContext());
        View userView=layoutInflater.inflate(R.layout.list_item_user,parent,false);
        MembersViewHolder membersViewHolder=new MembersViewHolder(userView);

        return membersViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MembersViewHolder holder, int position) {
        final User user = mUserArrayList.get(holder.getAdapterPosition());

        holder.tvUername.setText(user.username);
        holder.tvId.setText(user.id);
        holder.tvAge.setText(user.age);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ListActionUsersDialog listActionUsersDialog=new ListActionUsersDialog();

                Bundle bundle=new Bundle();
                bundle.putString("userId",user.id);
                bundle.putString("userName",user.username);
                bundle.putString(KEY_AGE,user.age);
                listActionUsersDialog.setArguments(bundle);

                listActionUsersDialog.show(mFragmentManager,"tag");

            }
        });

    }

    @Override
    public int getItemCount() {
        return mUserArrayList.size();
    }
}
