package com.icbt.libraryapp2.adapters.pizza;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.database.DatabaseReference;
import com.icbt.libraryapp2.R;
import com.icbt.libraryapp2.model.Pizza;

import java.util.ArrayList;

public class PizzaRecyclerAdapter extends RecyclerView.Adapter {
    ArrayList<Pizza> pizzaArrayList;

    public PizzaRecyclerAdapter(ArrayList<Pizza> pizzaArrayList) {
        this.pizzaArrayList = pizzaArrayList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater=LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.list_item_pizza,parent,false);
        return new PizzaViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Pizza pizza = pizzaArrayList.get(holder.getAdapterPosition());
        PizzaViewHolder pizzaViewHolder = (PizzaViewHolder) holder;
        pizzaViewHolder.tvPizzaPrice.setText(pizza.getSize());
        pizzaViewHolder.tvPizzaName.setText(pizza.getName());
    }

    @Override
    public int getItemCount() {
        return pizzaArrayList.size();
    }
}
