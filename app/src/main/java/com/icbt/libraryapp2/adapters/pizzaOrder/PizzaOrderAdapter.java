package com.icbt.libraryapp2.adapters.pizzaOrder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.icbt.libraryapp2.R;
import com.icbt.libraryapp2.model.Order;
import com.icbt.libraryapp2.model.Pizza;

import java.util.ArrayList;

public class PizzaOrderAdapter extends RecyclerView.Adapter {
    ArrayList<Order> pizzaOrderList;

    public PizzaOrderAdapter(ArrayList<Order> pizzaOrderList) {
        this.pizzaOrderList = pizzaOrderList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater=LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.list_item_pizza_order,parent,false);
        return new PizzaOrderViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Order order = pizzaOrderList.get(holder.getAdapterPosition());
        PizzaOrderViewHolder pizzaOrderViewHolder = (PizzaOrderViewHolder) holder;

    }

    @Override
    public int getItemCount() {
        return pizzaOrderList.size();
    }
}
